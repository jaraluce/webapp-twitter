package me.relevante.web.controller;

public class TwitterRoute {

    public static final String NETWORK = "/twitter";
    public static final String CONNECT = NETWORK + "/connect";
    public static final String CONNECT_CALLBACK = CONNECT + "/callback";
    public static final String DISCONNECT = NETWORK + "/disconnect";
    public static final String API_RETWEET = CoreRoute.API_PREFIX + "/actions/custom" + NETWORK + "/retweet";
}

