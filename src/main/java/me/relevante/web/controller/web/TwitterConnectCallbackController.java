package me.relevante.web.controller.web;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.TwitterRoute;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.service.TwitterAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class TwitterConnectCallbackController {

    @Autowired private TwitterAccountService accountService;

    @RequestMapping(value = TwitterRoute.CONNECT_CALLBACK, method = RequestMethod.GET)
    public ModelAndView get(@RequestParam(name = "oauth_token") String oAuthRequestOriginalToken,
                            @RequestParam(name = "oauth_verifier") String oAuthRequestVerifier,
                            HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        String oAuthRequestToken = (String) session.getAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN);
        String oAuthRequestSecret = (String) session.getAttribute(CoreSessionAttribute.OAUTH_REQUEST_SECRET);
        String redirectUri = (String) session.getAttribute(CoreSessionAttribute.REDIRECT_URI);
        RegisterResult result = accountService.registerOAuthNetworkAccount(relevanteId, oAuthRequestToken, oAuthRequestSecret, oAuthRequestOriginalToken, oAuthRequestVerifier);
        session.setAttribute(CoreSessionAttribute.RELEVANTE_ID, result.getRelevanteAccount().getId());

        return new ModelAndView("redirect:" + redirectUri);
    }

}
