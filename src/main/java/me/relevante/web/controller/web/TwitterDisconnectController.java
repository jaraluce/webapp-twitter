package me.relevante.web.controller.web;

import me.relevante.web.controller.CoreRoute;
import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.TwitterRoute;
import me.relevante.web.service.TwitterAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class TwitterDisconnectController {

    @Autowired private TwitterAccountService accountService;

    @RequestMapping(value = TwitterRoute.DISCONNECT, method = RequestMethod.GET)
    public ModelAndView get(HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        accountService.unregisterNetworkAccount(relevanteId);

        return new ModelAndView("redirect:" + CoreRoute.ACCOUNT);
    }
}
