package me.relevante.web.controller.web;

import me.relevante.api.OAuthKeyPair;
import me.relevante.network.Twitter;
import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.TwitterRoute;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.model.oauth.OAuthAccessRequest;
import me.relevante.web.model.oauth.TwitterOAuthManagerProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class TwitterConnectController {

    @Autowired @Qualifier(value = "twitterMainOAuthKeyPair")
    private OAuthKeyPair<Twitter> mainOAuthKeyPair;
    @Autowired @Qualifier(value = "twitterExtraOAuthKeyPair")
    private OAuthKeyPair<Twitter> extraOAuthKeyPair;
    @Autowired @Qualifier(value = "twitterEmergencyOAuthKeyPair")
    private OAuthKeyPair<Twitter> emergencyOAuthKeyPair;

    @RequestMapping(value = TwitterRoute.CONNECT, method = RequestMethod.GET)
    public ModelAndView get(@RequestParam(name = "redirectUri") String redirectUri,
                            HttpServletRequest request) {

        String baseUrl = request.getRequestURL().substring(0, request.getRequestURL().indexOf(TwitterRoute.CONNECT));
        String callbackUrl = baseUrl + TwitterRoute.CONNECT_CALLBACK;
        NetworkOAuthManager<Twitter> twitterOAuthManager = new TwitterOAuthManagerProxy(mainOAuthKeyPair, extraOAuthKeyPair, emergencyOAuthKeyPair);
        OAuthAccessRequest<Twitter> oAuthAccessRequest = twitterOAuthManager.createOAuthRequest(callbackUrl);
        HttpSession session = request.getSession();
        session.setAttribute(CoreSessionAttribute.OAUTH_REQUEST_TOKEN, oAuthAccessRequest.getRequestToken());
        session.setAttribute(CoreSessionAttribute.OAUTH_REQUEST_SECRET, oAuthAccessRequest.getRequestSecret());
        session.setAttribute(CoreSessionAttribute.REDIRECT_URI, redirectUri);

        return new ModelAndView("redirect:" + oAuthAccessRequest.getOAuthUrl());
    }

}
