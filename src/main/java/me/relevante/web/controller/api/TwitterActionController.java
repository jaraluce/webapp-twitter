package me.relevante.web.controller.api;

import me.relevante.web.controller.CoreSessionAttribute;
import me.relevante.web.controller.TwitterRoute;
import me.relevante.web.model.json.TwitterRetweetRequest;
import me.relevante.web.service.TwitterActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class TwitterActionController {

    @Autowired private TwitterActionService actionService;

    @RequestMapping(value = TwitterRoute.API_RETWEET,
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.TEXT_PLAIN_VALUE)
    public String reTweet(@RequestBody TwitterRetweetRequest retweetJson,
                          HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        String retweetResponse = actionService.postRetweet(relevanteId, retweetJson.getTweetId());
        return retweetResponse;
    }

}