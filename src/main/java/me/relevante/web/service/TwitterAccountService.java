package me.relevante.web.service;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.api.TwitterApiProxy;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.model.oauth.TwitterOAuthManagerProxy;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TwitterAccountService extends AbstractNetworkAccountService<Twitter, TwitterProfile, TwitterFullUser>
        implements NetworkAccountService<Twitter, TwitterFullUser> {

    private OAuthKeyPair<Twitter> mainOAuthKeyPair;
    private OAuthKeyPair<Twitter> extraOAuthKeyPair;
    private OAuthKeyPair<Twitter> emergencyOAuthKeyPair;

    @Autowired
    public TwitterAccountService(RelevanteAccountRepo relevanteAccountRepo,
                                 RelevanteContextRepo relevanteContextRepo,
                                 CrudRepository<TwitterFullUser, String> fullUserRepo,
                                 QueueService queueService,
                                 @Qualifier(value = "twitterMainOAuthKeyPair") OAuthKeyPair<Twitter> mainOAuthKeyPair,
                                 @Qualifier(value = "twitterExtraOAuthKeyPair") OAuthKeyPair<Twitter> extraOAuthKeyPair,
                                 @Qualifier(value = "twitterEmergencyOAuthKeyPair") OAuthKeyPair<Twitter> emergencyOAuthKeyPair) {
        super(relevanteAccountRepo, relevanteContextRepo, fullUserRepo, queueService, Twitter.getInstance());
        this.mainOAuthKeyPair = mainOAuthKeyPair;
        this.extraOAuthKeyPair = extraOAuthKeyPair;
        this.emergencyOAuthKeyPair = emergencyOAuthKeyPair;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public RegisterResult<TwitterFullUser> registerBasicAuthNetworkAccount(String relevanteId, String url, String username, String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected NetworkOAuthManager<Twitter> createNetworkOAuthManager() {
        return new TwitterOAuthManagerProxy(mainOAuthKeyPair, extraOAuthKeyPair, emergencyOAuthKeyPair);
    }

    @Override
    protected TwitterProfile createProfileFromOAuthData(OAuthTokenPair<Twitter> oAuthTokenPair) {
        return new TwitterApiProxy(mainOAuthKeyPair, extraOAuthKeyPair, emergencyOAuthKeyPair, oAuthTokenPair).getUserData();
    }

    @Override
    protected TwitterProfile createProfileFromBasicAuthData(String s, String s1, String s2) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected TwitterFullUser createFullUserFromProfile(TwitterProfile twitterProfile) {
        return new TwitterFullUser(twitterProfile);
    }

    @Override
    protected void populateRelevanteAccountWithNetworkData(RelevanteAccount relevanteAccount,
                                                           TwitterFullUser fullUser)  {

        if (relevanteAccount.getName() == null) {
            relevanteAccount.setName(fullUser.getProfile().getName());
        }
        if (relevanteAccount.getImageUrl() == null) {
            relevanteAccount.setImageUrl(fullUser.getProfile().getImageUrl());
        }
    }

}
