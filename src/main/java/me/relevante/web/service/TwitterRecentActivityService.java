package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterProfile;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Twitter;
import me.relevante.web.model.FullUserWithPostsComparator;
import me.relevante.web.model.filter.IsNotLoggedUserFilter;
import me.relevante.web.model.filter.OutOfBlacklistFilter;
import me.relevante.web.model.validation.TwitterFullUserValidator;
import me.relevante.web.persistence.TwitterFullPostRepo;
import me.relevante.web.persistence.TwitterFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class TwitterRecentActivityService implements NetworkRecentActivityService<Twitter, TwitterFullUser> {

    private FullUserWithPostsComparator<Twitter, TwitterFullUser> fullUserComparator;
    private TwitterFullUserRepo fullUserRepo;
    private TwitterFullPostRepo fullPostRepo;
    private TwitterFullUserValidator fullUserValidator;

    @Autowired
    public TwitterRecentActivityService(FullUserWithPostsComparator<Twitter, TwitterFullUser> fullUserComparator,
                                        TwitterFullUserRepo fullUserRepo,
                                        TwitterFullPostRepo fullPostRepo,
                                        TwitterFullUserValidator fullUserValidator) {
        this.fullUserComparator = fullUserComparator;
        this.fullUserRepo = fullUserRepo;
        this.fullPostRepo = fullPostRepo;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public List<TwitterFullUser> getNetworkRecentlyUpdatedUsers(RelevanteAccount relevanteAccount,
                                                                RelevanteContext relevanteContext) {

        List<String> relatedUserIds = relevanteContext.getRelatedUserIdsByNetwork(Twitter.getInstance());
        List<TwitterFullUser> twitterFullUsers = fullUserRepo.findByIdIn(relatedUserIds);

        assignTwitterPostsToUsers(twitterFullUsers);

        // Sort by most recent activity
        List<TwitterFullUser> sortedTwitterFullUsers = new ArrayList<>(twitterFullUsers);
        sortedTwitterFullUsers.sort(fullUserComparator);

        // Select users out of blacklist, validated and in pagination
        Blacklist blacklist = relevanteContext.getBlacklist();
        List<TwitterFullUser> selectedTwitterUsers = new FilterChain<TwitterFullUser>()
                .add(new OutOfBlacklistFilter(blacklist))
                .add(new IsNotLoggedUserFilter(relevanteAccount))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(sortedTwitterFullUsers);

        return selectedTwitterUsers;
    }

    private void assignTwitterPostsToUsers(List<TwitterFullUser> fullUsers) {
        Set<String> postIds = new HashSet();
        fullUsers.forEach(searchUser -> postIds.addAll(searchUser.getPostIds()));
        List<TwitterFullPost> posts = fullPostRepo.findByIdIn(new ArrayList<>(postIds));
        Map<String, TwitterFullPost> postMap = new HashMap<>();
        posts.forEach(post -> postMap.put(post.getId(), post));
        for (TwitterFullUser fullUser : fullUsers) {
            fullUser.getLastPosts().clear();
            for (String postId : fullUser.getPostIds()) {
                fullUser.getLastPosts().add(postMap.get(postId));
            }
        }
    }

}
