package me.relevante.web.service;

import me.relevante.model.ActionStatus;
import me.relevante.model.TwitterActionDirectMessage;
import me.relevante.model.TwitterActionFav;
import me.relevante.model.TwitterActionFollow;
import me.relevante.model.TwitterActionReply;
import me.relevante.model.TwitterActionRetweet;
import me.relevante.network.Twitter;
import me.relevante.web.model.json.NetworkActionJson;
import me.relevante.web.model.json.TwitterDirectMessageJson;
import me.relevante.web.model.json.TwitterFavJson;
import me.relevante.web.model.json.TwitterFollowJson;
import me.relevante.web.model.json.TwitterReplyJson;
import me.relevante.web.model.json.TwitterRetweetJson;
import me.relevante.web.persistence.TwitterActionDirectMessageRepo;
import me.relevante.web.persistence.TwitterActionFavRepo;
import me.relevante.web.persistence.TwitterActionFollowRepo;
import me.relevante.web.persistence.TwitterActionReplyRepo;
import me.relevante.web.persistence.TwitterActionRetweetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
@Service
public class TwitterPendingActionsService implements NetworkPendingActionsService<Twitter> {


    private TwitterActionFavRepo favRepo;
    private TwitterActionRetweetRepo retweetRepo;
    private TwitterActionReplyRepo replyRepo;
    private TwitterActionFollowRepo followRepo;
    private TwitterActionDirectMessageRepo directMessageRepo;

    @Autowired
    public TwitterPendingActionsService(TwitterActionFavRepo favRepo,
                                        TwitterActionRetweetRepo retweetRepo,
                                        TwitterActionReplyRepo replyRepo,
                                        TwitterActionFollowRepo followRepo,
                                        TwitterActionDirectMessageRepo directMessageRepo) {
        this.favRepo = favRepo;
        this.retweetRepo = retweetRepo;
        this.replyRepo = replyRepo;
        this.followRepo = followRepo;
        this.directMessageRepo = directMessageRepo;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public List<NetworkActionJson<Twitter>> getPendingActions(String userId) {
        List<NetworkActionJson<Twitter>> pendingActions = new ArrayList<>();
        List<TwitterFavJson> favs = getPendingFavs(userId);
        pendingActions.addAll(favs);
        List<TwitterRetweetJson> retweets = getPendingRetweets(userId);
        pendingActions.addAll(retweets);
        List<TwitterReplyJson> replies = getPendingReplies(userId);
        pendingActions.addAll(replies);
        List<TwitterFollowJson> follows = getPendingFollows(userId);
        pendingActions.addAll(follows);
        List<TwitterDirectMessageJson> directMessages = getPendingDirectMessages(userId);
        pendingActions.addAll(directMessages);
        return pendingActions;
    }

    @Override
    public void acknowledgeActionDone(String actionName, String actionId) {
        if (actionName.equals(TwitterFavJson.ACTION_NAME)) {
            TwitterActionFav fav = favRepo.findOne(actionId);
            fav.setSuccess();
            favRepo.save(fav);
        } else if (actionName.equals(TwitterRetweetJson.ACTION_NAME)) {
            TwitterActionRetweet retweet = retweetRepo.findOne(actionId);
            retweet.setSuccess();
            retweetRepo.save(retweet);
        } else if (actionName.equals(TwitterReplyJson.ACTION_NAME)) {
            TwitterActionReply reply = replyRepo.findOne(actionId);
            reply.setSuccess();
            replyRepo.save(reply);
        } else if (actionName.equals(TwitterFollowJson.ACTION_NAME)) {
            TwitterActionFollow follow = followRepo.findOne(actionId);
            follow.setSuccess();
            followRepo.save(follow);
        } else if (actionName.equals(TwitterDirectMessageJson.ACTION_NAME)) {
            TwitterActionDirectMessage directMessage = directMessageRepo.findOne(actionId);
            directMessage.setSuccess();
            directMessageRepo.save(directMessage);
        } else {
            throw new IllegalArgumentException("Invalid actionName received [" + actionName + "]");
        }
    }

    @Override
    public void acknowledgeActionError(String actionName, String actionId) {
        if (actionName.equals(TwitterFavJson.ACTION_NAME)) {
            TwitterActionFav fav = favRepo.findOne(actionId);
            fav.setError();
            favRepo.save(fav);
        } else if (actionName.equals(TwitterRetweetJson.ACTION_NAME)) {
            TwitterActionRetweet retweet = retweetRepo.findOne(actionId);
            retweet.setError();
            retweetRepo.save(retweet);
        } else if (actionName.equals(TwitterReplyJson.ACTION_NAME)) {
            TwitterActionReply reply = replyRepo.findOne(actionId);
            reply.setError();
            replyRepo.save(reply);
        } else if (actionName.equals(TwitterFollowJson.ACTION_NAME)) {
            TwitterActionFollow follow = followRepo.findOne(actionId);
            follow.setError();
            followRepo.save(follow);
        } else if (actionName.equals(TwitterDirectMessageJson.ACTION_NAME)) {
            TwitterActionDirectMessage directMessage = directMessageRepo.findOne(actionId);
            directMessage.setError();
            directMessageRepo.save(directMessage);
        } else {
            throw new IllegalArgumentException("Invalid actionName received [" + actionName + "]");
        }
    }

    private List<TwitterFavJson> getPendingFavs(String authorId) {
        List<TwitterActionFav> favs = favRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<TwitterFavJson> jsonFavs = new ArrayList<>();
        for (TwitterActionFav fav : favs) {
            TwitterFavJson favJson = new TwitterFavJson(fav.getId().toString(), fav.getPostId());
            jsonFavs.add(favJson);
        }
        return jsonFavs;
    }

    private List<TwitterRetweetJson> getPendingRetweets(String authorId) {
        List<TwitterActionRetweet> retweets = retweetRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<TwitterRetweetJson> retweetJsons = new ArrayList<>();
        for (TwitterActionRetweet retweet : retweets) {
            TwitterRetweetJson retweetJson = new TwitterRetweetJson(retweet.getId().toString(), retweet.getPostId());
            retweetJsons.add(retweetJson);
        }
        return retweetJsons;
    }

    private List<TwitterReplyJson> getPendingReplies(String authorId) {
        List<TwitterActionReply> replies = replyRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<TwitterReplyJson> replyJsons = new ArrayList<>();
        for (TwitterActionReply reply : replies) {
            TwitterReplyJson jsonComment = new TwitterReplyJson(reply.getId().toString(), reply.getPostId(), reply.getReplyText());
            replyJsons.add(jsonComment);
        }
        return replyJsons;
    }

    private List<TwitterFollowJson> getPendingFollows(String authorId) {
        List<TwitterActionFollow> follows = followRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<TwitterFollowJson> followJsons = new ArrayList<>();
        for (TwitterActionFollow follow : follows) {
            TwitterFollowJson followJson = new TwitterFollowJson(follow.getId().toString(), follow.getTargetUserId());
            followJsons.add(followJson);
        }
        return followJsons;
    }

    private List<TwitterDirectMessageJson> getPendingDirectMessages(String authorId) {
        List<TwitterActionDirectMessage> directMessages = directMessageRepo.findByAuthorIdAndStatusNot(authorId, ActionStatus.PROCESSED.name());
        List<TwitterDirectMessageJson> directMessageJsons = new ArrayList<>();
        for (TwitterActionDirectMessage directMessage : directMessages) {
            TwitterDirectMessageJson directMessageJson = new TwitterDirectMessageJson(directMessage.getId().toString(), directMessage.getTargetUserId(), directMessage.getMessage());
            directMessageJsons.add(directMessageJson);
        }
        return directMessageJsons;
    }

}
