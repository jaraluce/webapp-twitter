package me.relevante.web.service;

import me.relevante.model.Blacklist;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.TwitterActionDirectMessage;
import me.relevante.model.TwitterActionFav;
import me.relevante.model.TwitterActionFollow;
import me.relevante.model.TwitterActionReply;
import me.relevante.model.TwitterActionRetweet;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Twitter;
import me.relevante.web.model.filter.OutOfBlacklistFilter;
import me.relevante.web.model.validation.TwitterFullUserValidator;
import me.relevante.web.persistence.TwitterActionDirectMessageRepo;
import me.relevante.web.persistence.TwitterActionFavRepo;
import me.relevante.web.persistence.TwitterActionFollowRepo;
import me.relevante.web.persistence.TwitterActionReplyRepo;
import me.relevante.web.persistence.TwitterActionRetweetRepo;
import me.relevante.web.persistence.TwitterFullPostRepo;
import me.relevante.web.persistence.TwitterFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TwitterWatchlistUserService implements NetworkWatchlistUserService<Twitter, TwitterFullUser> {

    private TwitterFullUserRepo fullUserRepo;
    private TwitterFullPostRepo fullPostRepo;
    private TwitterActionFollowRepo followRepo;
    private TwitterActionDirectMessageRepo directMessageRepo;
    private TwitterActionFavRepo favRepo;
    private TwitterActionRetweetRepo retweetRepo;
    private TwitterActionReplyRepo replyRepo;
    private TwitterFullUserValidator fullUserValidator;

    @Autowired
    public TwitterWatchlistUserService(TwitterFullUserRepo fullUserRepo,
                                       TwitterFullPostRepo fullPostRepo,
                                       TwitterActionFollowRepo followRepo,
                                       TwitterActionDirectMessageRepo directMessageRepo,
                                       TwitterActionFavRepo favRepo,
                                       TwitterActionRetweetRepo retweetRepo,
                                       TwitterActionReplyRepo replyRepo,
                                       TwitterFullUserValidator fullUserValidator) {
        this.fullUserRepo = fullUserRepo;
        this.fullPostRepo = fullPostRepo;
        this.followRepo = followRepo;
        this.directMessageRepo = directMessageRepo;
        this.favRepo = favRepo;
        this.retweetRepo = retweetRepo;
        this.replyRepo = replyRepo;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public List<TwitterFullUser> getNetworkWatchlistUsers(RelevanteAccount relevanteAccount,
                                                          RelevanteContext relevanteContext,
                                                          String watchlistId) {
        // Get full users in this watchlist ID
        List<String> watchlistUserIds = relevanteContext.getWatchlistUserIdsInWatchlistByNetwork(watchlistId, Twitter.getInstance());
        List<TwitterFullUser> fullUsers = fullUserRepo.findByIdIn(watchlistUserIds);

        // Select users out of blacklist & validated
        Blacklist blacklist = relevanteContext.getBlacklist();
        List<TwitterFullUser> selectedTwitterUsers = new FilterChain<TwitterFullUser>()
                .add(new OutOfBlacklistFilter(blacklist))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(fullUsers);

        Map<String, TwitterFullPost> fullPostsById = assignFullPostsToFullUsers(selectedTwitterUsers);
        if (relevanteAccount.isNetworkConnected(Twitter.getInstance())) {
            String relevanteUserTwitterId = relevanteAccount.getCredentials(Twitter.getInstance()).getUserId();
            populateFullUsersWithActions(selectedTwitterUsers, relevanteUserTwitterId);
            populateFullPostsWithActions(fullPostsById, relevanteUserTwitterId);
        }

        return selectedTwitterUsers;
    }

    private Map<String, TwitterFullPost> assignFullPostsToFullUsers(List<TwitterFullUser> fullUsers) {
        Set<String> postIds = new HashSet();
        fullUsers.forEach(searchUser -> postIds.addAll(searchUser.getPostIds()));
        List<TwitterFullPost> posts = fullPostRepo.findByIdIn(new ArrayList<>(postIds));
        Map<String, TwitterFullPost> fullPostsById = new HashMap<>();
        posts.forEach(post -> fullPostsById.put(post.getId(), post));
        for (TwitterFullUser fullUser : fullUsers) {
            fullUser.getLastPosts().clear();
            for (String postId : fullUser.getPostIds()) {
                fullUser.getLastPosts().add(fullPostsById.get(postId));
            }
        }
        return fullPostsById;
    }

    private void populateFullUsersWithActions(List<TwitterFullUser> fullUsers,
                                              String relevanteUserTwitterId) {
        List<String> userIds = fullUsers.stream().map(fullUser -> fullUser.getId()).collect(Collectors.toList());
        List<TwitterActionFollow> follows = followRepo.findByAuthorIdAndTargetUserIdIn(relevanteUserTwitterId, userIds);
        List<TwitterActionDirectMessage> directMessages = directMessageRepo.findByAuthorIdAndTargetUserIdIn(relevanteUserTwitterId, userIds);
        Map<String, TwitterFullUser> fullUsersById = new HashMap<>();
        fullUsers.forEach(fullUser -> fullUsersById.put(fullUser.getId(), fullUser));
        follows.forEach(follow -> fullUsersById.get(follow.getTargetUserId()).getFollows().add(follow));
        directMessages.forEach(directMessage -> fullUsersById.get(directMessage.getTargetUserId()).getDirectMessages().add(directMessage));
    }

    private void populateFullPostsWithActions(Map<String, TwitterFullPost> fullPostsById,
                                              String relevanteUserTwitterId) {
        List<String> postIds = fullPostsById.values().stream().map(fullPost -> fullPost.getId()).collect(Collectors.toList());
        List<TwitterActionFav> favs = favRepo.findByAuthorIdAndPostIdIn(relevanteUserTwitterId, postIds);
        List<TwitterActionRetweet> retweets = retweetRepo.findByAuthorIdAndPostIdIn(relevanteUserTwitterId, postIds);
        List<TwitterActionReply> replies = replyRepo.findByAuthorIdAndPostIdIn(relevanteUserTwitterId, postIds);
        favs.forEach(fav -> fullPostsById.get(fav.getPostId()).getFavs().add(fav));
        retweets.forEach(retweet -> fullPostsById.get(retweet.getPostId()).getRetweets().add(retweet));
        replies.forEach(reply -> fullPostsById.get(reply.getPostId()).getReplies().add(reply));
    }

}
