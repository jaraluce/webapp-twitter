package me.relevante.web.service;

import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.filter.FilterChain;
import me.relevante.model.filter.ValidationFilter;
import me.relevante.network.Twitter;
import me.relevante.web.model.FullUserWithPostsComparator;
import me.relevante.web.model.filter.IsNotLoggedUserFilter;
import me.relevante.web.model.validation.TwitterFullUserValidator;
import me.relevante.web.persistence.TwitterFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TwitterBlacklistUserService implements NetworkBlacklistUserService<Twitter, TwitterFullUser> {

    private TwitterFullUserRepo fullUserRepo;
    private FullUserWithPostsComparator<Twitter, TwitterFullUser> fullUserComparator;
    private TwitterFullUserValidator fullUserValidator;

    @Autowired
    public TwitterBlacklistUserService(TwitterFullUserRepo fullUserRepo,
                                       FullUserWithPostsComparator<Twitter, TwitterFullUser> fullUserComparator,
                                       TwitterFullUserValidator fullUserValidator) {
        this.fullUserRepo = fullUserRepo;
        this.fullUserComparator = fullUserComparator;
        this.fullUserValidator = fullUserValidator;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public List<TwitterFullUser> getNetworkBlacklistUsers(RelevanteAccount relevanteAccount,
                                                          RelevanteContext relevanteContext) {

        List<String> relatedUserIds = relevanteContext.getBlacklistUserIdsByNetwork(Twitter.getInstance());
        List<TwitterFullUser> fullUsers = fullUserRepo.findByIdIn(relatedUserIds);

        // Sort by most recent activity
        List<TwitterFullUser> sortedFullUsers = new ArrayList<>(fullUsers);
        sortedFullUsers.sort(fullUserComparator);

        // Select users out of blacklist, validated and in pagination
        List<TwitterFullUser> selectedUsers = new FilterChain<TwitterFullUser>()
                .add(new IsNotLoggedUserFilter(relevanteAccount))
                .add(new ValidationFilter<>(fullUserValidator))
                .execute(sortedFullUsers);

        return selectedUsers;

    }

}
