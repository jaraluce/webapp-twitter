package me.relevante.web.service;

import me.relevante.api.NetworkCredentials;
import me.relevante.model.ActionResult;
import me.relevante.model.ActionStatus;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.TwitterActionDirectMessage;
import me.relevante.model.TwitterActionFav;
import me.relevante.model.TwitterActionFollow;
import me.relevante.model.TwitterActionReply;
import me.relevante.model.TwitterActionRetweet;
import me.relevante.network.Twitter;
import me.relevante.web.model.ActionProgressOutput;
import me.relevante.web.model.ActionServiceResponse;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.TwitterActionDirectMessageRepo;
import me.relevante.web.persistence.TwitterActionFavRepo;
import me.relevante.web.persistence.TwitterActionFollowRepo;
import me.relevante.web.persistence.TwitterActionReplyRepo;
import me.relevante.web.persistence.TwitterActionRetweetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TwitterActionService implements NetworkActionService<Twitter> {

    private RelevanteAccountRepo relevanteAccountRepo;
    private TwitterActionFavRepo favRepo;
    private TwitterActionReplyRepo replyRepo;
    private TwitterActionRetweetRepo retweetRepo;
    private TwitterActionFollowRepo followRepo;
    private TwitterActionDirectMessageRepo directMessageRepo;
    private ActionProgressOutput actionProgressOutput;
    private QueueService queueService;

    @Autowired
    public TwitterActionService(RelevanteAccountRepo relevanteAccountRepo,
                                TwitterActionFavRepo favRepo,
                                TwitterActionReplyRepo replyRepo,
                                TwitterActionRetweetRepo retweetRepo,
                                TwitterActionFollowRepo followRepo,
                                TwitterActionDirectMessageRepo directMessageRepo,
                                ActionProgressOutput actionProgressOutput,
                                QueueService queueService) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.favRepo = favRepo;
        this.replyRepo = replyRepo;
        this.retweetRepo = retweetRepo;
        this.followRepo = followRepo;
        this.directMessageRepo = directMessageRepo;
        this.actionProgressOutput = actionProgressOutput;
        this.queueService = queueService;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public String putLevel1Action(String relevanteId,
                                  String postId) {
        return putFav(relevanteId, postId);
    }

    @Override
    public String postLevel2Action(String relevanteId,
                                   String postId,
                                   String commentText) {
        return postReply(relevanteId, postId, commentText);
    }

    @Override
    public String putLevel3Action(String relevanteId,
                                  String targetUserId) {
        return putFollow(relevanteId, targetUserId);
    }

    @Override
    public String postLevel4Action(String relevanteId,
                                   String targetUserId,
                                   String subject,
                                   String message) {
        return postDirectMessage(relevanteId, targetUserId, message);
    }

    public String postRetweet(String relevanteId,
                              String tweetId) {
        ActionServiceResponse serviceResponse = processRetweet(relevanteId, tweetId);
        return actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
    }

    public String putFav(String relevanteId,
                         String tweetId) {
        ActionServiceResponse serviceResponse = processFav(relevanteId, tweetId);
        String output = actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
        return output;
    }

    public String postReply(String relevanteId,
                            String tweetId,
                            String commentText) {
        ActionServiceResponse serviceResponse = processReply(relevanteId, tweetId, commentText);
        String output = actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
        return output;
    }

    public String putFollow(String relevanteId,
                            String targetUserId) {
        ActionServiceResponse serviceResponse = processFollow(relevanteId, targetUserId);
        String output = actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
        return output;
    }

    public String postDirectMessage(String relevanteId,
                                    String targetUserId,
                                    String message) {
        ActionServiceResponse serviceResponse = processDirectMessage(relevanteId, targetUserId, message);
        String output = actionProgressOutput.getOutput(serviceResponse.getStatus(), serviceResponse.getResult());
        return output;
    }

    private ActionServiceResponse processFav(String relevanteId,
                                             String tweetId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Twitter> credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<TwitterActionFav> existingTweetFavs = favRepo.findByAuthorIdAndPostId(credentials.getUserId(), tweetId);
        if (existingTweetFavs.size() > 0) {
            return new ActionServiceResponse(existingTweetFavs.get(0));
        }

        TwitterActionFav newTweetFav = new TwitterActionFav(credentials.getUserId(), tweetId);
        newTweetFav = favRepo.save(newTweetFav);
        queueService.sendBulkActionsMessage(relevanteId, TwitterActionFav.class, newTweetFav.getId());
        return new ActionServiceResponse(newTweetFav);
    }

    private ActionServiceResponse processReply(String relevanteId,
                                               String tweetId,
                                               String text) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Twitter> credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<TwitterActionReply> existingTweetReplies = replyRepo.findByAuthorIdAndPostId(credentials.getUserId(), tweetId);
        if (existingTweetReplies.size() > 0) {
            return new ActionServiceResponse(existingTweetReplies.get(0));
        }

        TwitterActionReply newTweetReply = new TwitterActionReply(credentials.getUserId(), tweetId, text);
        newTweetReply = replyRepo.save(newTweetReply);
        queueService.sendBulkActionsMessage(relevanteId, TwitterActionReply.class, newTweetReply.getId());
        return new ActionServiceResponse(newTweetReply);
    }

    public ActionServiceResponse processRetweet(String relevanteId,
                                                String tweetId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Twitter> credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<TwitterActionRetweet> existingRetweets = retweetRepo.findByAuthorIdAndPostId(credentials.getUserId(), tweetId);
        if (existingRetweets.size() > 0) {
            return new ActionServiceResponse(existingRetweets.get(0));
        }

        TwitterActionRetweet newRetweet = new TwitterActionRetweet(credentials.getUserId(), tweetId);
        newRetweet = retweetRepo.save(newRetweet);
        queueService.sendBulkActionsMessage(relevanteId, TwitterActionRetweet.class, newRetweet.getId());
        return new ActionServiceResponse(newRetweet);
    }

    private ActionServiceResponse processFollow(String relevanteId,
                                                String targetUserId) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Twitter> credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<TwitterActionFollow> existingFollows = followRepo.findByAuthorIdAndTargetUserId(credentials.getUserId(), targetUserId);
        if (existingFollows.size() > 0) {
            return new ActionServiceResponse(existingFollows.get(0));
        }

        TwitterActionFollow newFollow = new TwitterActionFollow(credentials.getUserId(), targetUserId);
        newFollow = followRepo.save(newFollow);
        queueService.sendBulkActionsMessage(relevanteId, TwitterActionFollow.class, newFollow.getId());
        return new ActionServiceResponse(newFollow);
    }

    private ActionServiceResponse processDirectMessage(String relevanteId,
                                                       String targetUserId,
                                                       String message) {

        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkCredentials<Twitter> credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return new ActionServiceResponse(ActionStatus.PROCESSED, ActionResult.ERROR);
        }

        List<TwitterActionDirectMessage> existingDirectMessages = directMessageRepo.findByAuthorIdAndTargetUserId(credentials.getUserId(), targetUserId);
        if (existingDirectMessages.size() > 0) {
            return new ActionServiceResponse(existingDirectMessages.get(0));
        }

        TwitterActionDirectMessage newDirectMessage = new TwitterActionDirectMessage(credentials.getUserId(), targetUserId, message);
        newDirectMessage = directMessageRepo.save(newDirectMessage);
        queueService.sendBulkActionsMessage(relevanteId, TwitterActionDirectMessage.class, newDirectMessage.getId());
        return new ActionServiceResponse(newDirectMessage);
    }

}
