package me.relevante.web.persistence;

import me.relevante.network.Twitter;
import me.relevante.nlp.TwitterStemUserIndex;
import me.relevante.persistence.NetworkIndexRepo;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TwitterStemUserIndexRepo extends NetworkIndexRepo<Twitter, TwitterStemUserIndex> {
    List<TwitterStemUserIndex> findByStemOrderByScoreDesc(String stem, Pageable pageable);
}