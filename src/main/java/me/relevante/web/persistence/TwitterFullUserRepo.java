package me.relevante.web.persistence;

import me.relevante.model.TwitterFullUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TwitterFullUserRepo extends MongoRepository<TwitterFullUser, String> {

    List<TwitterFullUser> findByIdIn(List<String> ids);

}