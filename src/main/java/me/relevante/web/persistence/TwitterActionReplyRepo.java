package me.relevante.web.persistence;

import me.relevante.model.TwitterActionReply;
import me.relevante.network.Twitter;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TwitterActionReplyRepo extends NetworkPostActionRepo<Twitter, TwitterActionReply>, MongoRepository<TwitterActionReply, String> {
	List<TwitterActionReply> findByAuthorIdAndPostId(String authorId, String tweetId);
	List<TwitterActionReply> findByAuthorIdAndPostIdIn(String authorId, Collection<String> postIds);
	TwitterActionReply save(TwitterActionReply reply);
	List<TwitterActionReply> findByAuthorIdAndStatusNot(String authorId, String status);
}
