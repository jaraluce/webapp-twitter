package me.relevante.web.persistence;

import me.relevante.model.TwitterSearchUser;
import me.relevante.network.Twitter;
import me.relevante.persistence.NetworkSearchUserRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TwitterSearchUserRepo extends NetworkSearchUserRepo<Twitter, TwitterSearchUser>, MongoRepository<TwitterSearchUser, String> {
    List<TwitterSearchUser> findByIdIn(List<String> ids);
}