package me.relevante.web.persistence;

import me.relevante.model.TwitterActionFollow;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TwitterActionFollowRepo extends MongoRepository<TwitterActionFollow, String> {

	List<TwitterActionFollow> findByAuthorIdAndTargetUserId(String authorId, String targetUserId);
	List<TwitterActionFollow> findByAuthorIdAndTargetUserIdIn(String authorId, Collection<String> targetUserIds);
	TwitterActionFollow save(TwitterActionFollow follow);
	List<TwitterActionFollow> findByAuthorIdAndStatusNot(String authorId, String status);
}
