package me.relevante.web.persistence;

import me.relevante.model.TwitterActionFav;
import me.relevante.network.Twitter;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TwitterActionFavRepo extends NetworkPostActionRepo<Twitter, TwitterActionFav>, MongoRepository<TwitterActionFav, String> {
	List<TwitterActionFav> findByAuthorIdAndPostId(String authorId, String tweetId);
	List<TwitterActionFav> findByAuthorIdAndPostIdIn(String authorId, Collection<String> tweetIds);
	TwitterActionFav save(TwitterActionFav fav);
	List<TwitterActionFav> findByAuthorIdAndStatusNot(String authorId, String status);
}
