package me.relevante.web.persistence;

import me.relevante.model.TwitterFullPost;
import me.relevante.persistence.NetworkFindByIdsRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterFullPostRepo extends NetworkFullPostRepo<TwitterFullPost>, NetworkFindByIdsRepo<TwitterFullPost> {
}
