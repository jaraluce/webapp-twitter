package me.relevante.web.persistence;

import me.relevante.model.TwitterActionRetweet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TwitterActionRetweetRepo extends MongoRepository<TwitterActionRetweet, String> {

	List<TwitterActionRetweet> findByAuthorIdAndPostId(String authorId, String tweetId);
	List<TwitterActionRetweet> findByAuthorIdAndPostIdIn(String authorId, Collection<String> postIds);
	TwitterActionRetweet save(TwitterActionRetweet retweet);
	List<TwitterActionRetweet> findByAuthorIdAndStatusNot(String authorId, String status);
}
