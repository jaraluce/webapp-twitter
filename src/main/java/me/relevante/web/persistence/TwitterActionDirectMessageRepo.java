package me.relevante.web.persistence;

import me.relevante.model.TwitterActionDirectMessage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TwitterActionDirectMessageRepo extends MongoRepository<TwitterActionDirectMessage, String> {

	List<TwitterActionDirectMessage> findByAuthorIdAndTargetUserId(String authorId, String targetUserId);
	List<TwitterActionDirectMessage> findByAuthorIdAndTargetUserIdIn(String authorId, Collection<String> targetUserIds);
    TwitterActionDirectMessage save(TwitterActionDirectMessage directMessage);
	List<TwitterActionDirectMessage> findByAuthorIdAndStatusNot(String authorId, String status);
}
