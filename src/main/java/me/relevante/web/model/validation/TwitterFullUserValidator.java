package me.relevante.web.model.validation;

import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
@Component
public class TwitterFullUserValidator extends AbstractNetworkFullUserValidator<Twitter, TwitterFullUser, TwitterProfile>
        implements NetworkFullUserValidator<Twitter, TwitterFullUser> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Autowired
    public TwitterFullUserValidator(TwitterProfileValidator wrappedValidator) {
        super(wrappedValidator);
    }
}
