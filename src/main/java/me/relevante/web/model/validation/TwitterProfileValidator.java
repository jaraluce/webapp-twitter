package me.relevante.web.model.validation;

import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 19/07/16.
 */
@Component
public class TwitterProfileValidator implements NetworkProfileValidator<Twitter, TwitterProfile> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public boolean isValid(TwitterProfile profile) {
        if (profile == null) {
            return false;
        }
        if (StringUtils.isBlank(profile.getName())) {
            return false;
        }
        if (StringUtils.isBlank(profile.getScreenName())) {
            return false;
        }
        if (StringUtils.isBlank(profile.getProfileUrl())) {
            return false;
        }
        return true;
    }
}
