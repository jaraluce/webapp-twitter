package me.relevante.web.model.validation;

import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterSearchUser;
import me.relevante.network.Twitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by daniel-ibanez on 21/07/16.
 */
@Component
public class TwitterSearchUserValidator extends AbstractNetworkSearchUserValidator<Twitter, TwitterSearchUser, TwitterFullUser>
        implements NetworkSearchUserValidator<Twitter, TwitterSearchUser> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Autowired
    public TwitterSearchUserValidator(TwitterFullUserValidator wrappedValidator) {
        super(wrappedValidator);
    }
}
