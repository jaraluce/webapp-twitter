package me.relevante.web.model.card;

import me.relevante.api.NetworkCredentials;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterPost;
import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import me.relevante.web.model.json.card.TwitterCardPost;
import me.relevante.web.model.json.card.TwitterCardUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TwitterFullCardMapper extends AbstractNetworkFullCardMapper<Twitter, TwitterFullUser, TwitterCardUser>
        implements NetworkFullCardMapper<Twitter, TwitterFullUser, TwitterCardUser> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
	public TwitterCardUser mapFullUser(TwitterFullUser fullUser,
                                       RelevanteAccount relevanteAccount,
                                       RelevanteContext relevanteContext) {

        TwitterCardUser cardUser = mapProfile(fullUser.getProfile(), relevanteContext);
        cardUser.setId(fullUser.getId());
        cardUser.getKeywords().addAll(getCardRelatedTerms(fullUser.getRelatedTerms()));
        for (TwitterFullPost fullPost : fullUser.getLastPosts()) {
            TwitterCardPost cardPost = mapPost(fullPost.getPost());
            if (cardPost != null) {
                cardUser.getShares().add(cardPost);
                assignPostEngageProperties(cardPost, fullPost, relevanteAccount);
            }
        }
        assignUserEngageProperties(cardUser, fullUser, relevanteAccount);

        return cardUser;
    }

    public TwitterCardUser mapProfile(TwitterProfile user,
                                      RelevanteContext relevanteContext) {

        TwitterCardUser cardUser = new TwitterCardUser();
        cardUser.setType(user.getNetwork().getName());
        cardUser.setIsContact(relevanteContext.isContact(user));
        cardUser.getNotes().addAll(mapTags(relevanteContext.getTagNamesByUser(user)));
        cardUser.getWatchlists().addAll(mapWatchlists(relevanteContext.getWatchlistNamesByUser(user)));

        cardUser.setName(StringUtils.defaultString(user.getName()));
        cardUser.setScreenName(StringUtils.defaultString(user.getScreenName()));
        cardUser.setHeadline(StringUtils.defaultString(user.getDescription()));
        cardUser.setUrl(StringUtils.defaultString(user.getProfileUrl()));
        cardUser.setImg(StringUtils.defaultString(user.getImageUrl()));
        cardUser.setLocation(StringUtils.defaultString(user.getLocation()));
        cardUser.setUserBio(StringUtils.defaultString(user.getBioDescription()));
        cardUser.setUserBioUrl(StringUtils.defaultString(user.getBioUrl()));
        cardUser.setFollowers(StringUtils.defaultString(user.getFollowers()));
        cardUser.setFollowing(StringUtils.defaultString(user.getFollowing()));

        return cardUser;
    }

    private void assignUserEngageProperties(TwitterCardUser cardUser,
                                            TwitterFullUser fullUser,
                                            RelevanteAccount relevanteAccount) {

        NetworkCredentials credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return;
        }
        cardUser.setFollow(networkActionProgressOutput.getOutput(fullUser.findFollowsByAuthorId(credentials.getUserId())));
        cardUser.setDirectMessage(networkActionProgressOutput.getOutput(fullUser.findDirectMessagesByAuthorId(credentials.getUserId())));
        for (TwitterFullPost fullPost : fullUser.getLastPosts()) {
            if (fullPost.findFavsByAuthorId(credentials.getUserId()).size() > 0) {
                cardUser.setTapDone(true);
            }
            if (fullPost.findRetweetsByAuthorId(credentials.getUserId()).size() > 0) {
                cardUser.setTapDone(true);
            }
            if (fullPost.findRepliesByAuthorId(credentials.getUserId()).size() > 0) {
                cardUser.setTouchDone(true);
            }
        }
    }

    private void assignPostEngageProperties(TwitterCardPost cardPost,
                                            TwitterFullPost fullPost,
                                            RelevanteAccount relevanteAccount) {

        NetworkCredentials credentials = relevanteAccount.getCredentials(Twitter.getInstance());
        if (credentials == null) {
            return;
        }
        cardPost.setReply(networkActionProgressOutput.getOutput(fullPost.findRepliesByAuthorId(credentials.getUserId())));
        cardPost.setRetweet(networkActionProgressOutput.getOutput(fullPost.findRetweetsByAuthorId(credentials.getUserId())));
        cardPost.setFav(networkActionProgressOutput.getOutput(fullPost.findFavsByAuthorId(credentials.getUserId())));
    }

    public TwitterCardPost mapPost(TwitterPost post) {

        TwitterCardPost cardPost = new TwitterCardPost();
        cardPost.setId(post.getId());
        cardPost.setDate(dateFormatter.formatDate(post.getCreationTimestamp()));
        cardPost.setLatitude(String.valueOf(post.getLatitude()));
        cardPost.setLongitude(String.valueOf(post.getLongitude()));
        cardPost.setLang(StringUtils.defaultString(post.getLanguage()));
        cardPost.setText(StringUtils.defaultString(post.getText()));
        cardPost.setUrl(StringUtils.defaultString(post.getTweetUrl()));
        cardPost.setImageUrl(null);

        return cardPost;
    }

}
