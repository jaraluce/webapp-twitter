package me.relevante.web.model.card;

import me.relevante.model.NetworkSignal;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.model.SignalCategory;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterPost;
import me.relevante.model.TwitterSearchUser;
import me.relevante.model.TwitterSignalBioDescription;
import me.relevante.model.TwitterSignalDescription;
import me.relevante.model.TwitterSignalPostAuthorship;
import me.relevante.model.TwitterSignalPostFav;
import me.relevante.model.TwitterSignalPostReply;
import me.relevante.model.TwitterSignalPostRetweet;
import me.relevante.network.Twitter;
import me.relevante.web.model.json.card.CardSignal;
import me.relevante.web.model.json.card.TwitterCardPost;
import me.relevante.web.model.json.card.TwitterCardUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TwitterSearchCardMapper extends AbstractNetworkSearchCardMapper<Twitter, TwitterSearchUser, TwitterCardUser>
        implements NetworkSearchCardMapper<Twitter, TwitterSearchUser, TwitterCardUser> {

    private static final int MAX_SIGNALS = 4;

    @Autowired private TwitterFullCardMapper fullCardMapper;

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public TwitterCardUser mapSearchUser(TwitterSearchUser searchUser,
                                         RelevanteAccount relevanteAccount,
                                         RelevanteContext relevanteContext) {
        if (searchUser == null) {
            return null;
        }
        TwitterCardUser cardUser = fullCardMapper.mapFullUser(searchUser.getFullUser(), relevanteAccount, relevanteContext);
        cardUser.getSignals().addAll(mapSignals(searchUser));
        cardUser.getShares().clear();
        for (TwitterFullPost fullPost : searchUser.getPosts()) {
            TwitterCardPost cardPost = mapPost(fullPost.getPost());
            if (cardPost != null) {
                cardUser.getShares().add(cardPost);
            }
        }
        cardUser.getShares().sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return cardUser;
    }

    public TwitterCardPost mapPost(TwitterPost post) {

        if (post == null) {
            return null;
        }

        TwitterCardPost cardPost = new TwitterCardPost();
        cardPost.setId(post.getId());
        cardPost.setDate(dateFormatter.formatDate(post.getCreationTimestamp()));
        cardPost.setLatitude(String.valueOf(post.getLatitude()));
        cardPost.setLongitude(String.valueOf(post.getLongitude()));
        cardPost.setLang(StringUtils.defaultString(post.getLanguage()));
        cardPost.setText(StringUtils.defaultString(post.getText()));
        cardPost.setUrl(StringUtils.defaultString(post.getTweetUrl()));
        cardPost.setImageUrl(null);

        return cardPost;
    }

    private List<CardSignal> mapSignals(TwitterSearchUser searchUser) {
        // List to return
        List<CardSignal> cardSignals = new ArrayList<>();

        // Create a map with one entry per category.
        // Each value is the list of that category's signals
        Map<SignalCategory, List<NetworkSignal>> signalsByCategory = new HashMap<>();
        for (SignalCategory signalCategory : SignalCategory.values()) {
            signalsByCategory.put(signalCategory, new ArrayList<>());
        }
        for (NetworkSignal signal : searchUser.getSignals()) {
            List<NetworkSignal> categorySignals = signalsByCategory.get(signal.getCategory());
            categorySignals.add(signal);
        }

        // Loop through the map, moving one signal from each category to the list
        boolean isAllProcessed = false;
        while (!isAllProcessed) {
            isAllProcessed = true;
            for (SignalCategory signalCategory : SignalCategory.values()) {
                List<NetworkSignal> categorySignals = signalsByCategory.get(signalCategory);
                if (!categorySignals.isEmpty()) {
                    NetworkSignal signal = categorySignals.get(0);
                    CardSignal cardSignal = mapSignal(signal, searchUser);
                    cardSignals.add(cardSignal);
                    categorySignals.remove(0);
                    if (!categorySignals.isEmpty()) {
                        isAllProcessed = false;
                    }
                }
            }
        }

        cardSignals = cardSignals.subList(0, Math.min(MAX_SIGNALS, cardSignals.size()));

        return cardSignals;
    }

    private CardSignal mapSignal(NetworkSignal signal,
                                 TwitterSearchUser searchUser) {
        CardSignal cardSignal = new CardSignal();
        String category = mapSignalCategory(signal.getCategory());
        String type = signal.getClass().getSimpleName().substring(14);
        String description = mapSignalDescription(signal, searchUser);
        cardSignal.setCategory(category);
        cardSignal.setType(type);
        cardSignal.setDescription(description);
        return cardSignal;
    }

    private String mapSignalCategory(SignalCategory category) {
        if (category.equals(SignalCategory.CONTENT_ACTIVITY)) {
            return "social";
        }
        else if (category.equals(SignalCategory.SELF_DESCRIPTION)) {
            return "bio";
        }
        else if (category.equals(SignalCategory.NETWORK)) {
            return "graph";
        }
        else if (category.equals(SignalCategory.OWNERSHIP)) {
            return "habitat";
        }
        return "";
    }

    private String mapSignalDescription(NetworkSignal signal,
                                        TwitterSearchUser searchUser) {
        Class signalClass = signal.getClass();
        if (signalClass.equals(TwitterSignalBioDescription.class)) {
            return searchUser.getFullUser().getProfile().getBioDescription();
        }
        else if (signalClass.equals(TwitterSignalDescription.class)) {
            return searchUser.getFullUser().getProfile().getDescription();
        }
        else if (signalClass.equals(TwitterSignalPostAuthorship.class)
                || signalClass.equals(TwitterSignalPostFav.class)
                || signalClass.equals(TwitterSignalPostRetweet.class)
                || signalClass.equals(TwitterSignalPostReply.class)) {
            for (TwitterFullPost fullPost : searchUser.getPosts()) {
                if (fullPost.getId().equals(signal.getRelatedEntityId())) {
                    String description = fullPost.getPost().getText();
                    return description;
                }
            }
            return null;
        }
        return "";
    }

}
