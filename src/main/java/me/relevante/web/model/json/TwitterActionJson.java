package me.relevante.web.model.json;

import me.relevante.network.Twitter;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterActionJson extends NetworkAbstractActionJson<Twitter> implements NetworkActionJson<Twitter> {

    public TwitterActionJson(String id) {
        super(id);
        this.network = Twitter.getInstance().getName();
    }
}
