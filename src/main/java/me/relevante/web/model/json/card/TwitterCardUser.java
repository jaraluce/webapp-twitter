package me.relevante.web.model.json.card;

import me.relevante.network.Twitter;

import java.util.ArrayList;
import java.util.List;

public class TwitterCardUser extends AbstractNetworkCardUser<Twitter> implements NetworkCardUser<Twitter> {

    private String name;
    private String screen_name;
    private String headline;
    private String url;
    private String img;
    private String location;
    private String userBio;
    private String userBioUrl;
    private String followers;
    private String following;
    private List<TwitterCardPost> shares;
    private String follow;
    private String directMessage;

    public TwitterCardUser() {
        super();
        this.shares = new ArrayList<>();
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screen_name;
    }

    public void setScreenName(String screen_name) {
        this.screen_name = screen_name;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserBio() {
        return userBio;
    }

    public void setUserBio(String userBio) {
        this.userBio = userBio;
    }

    public String getUserBioUrl() {
        return userBioUrl;
    }

    public void setUserBioUrl(String userBioUrl) {
        this.userBioUrl = userBioUrl;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public List<TwitterCardPost> getShares() {
        return shares;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public String getDirectMessage() {
        return directMessage;
    }

    public void setDirectMessage(String directMessage) {
        this.directMessage = directMessage;
    }
}
