package me.relevante.web.model.json;

import me.relevante.network.Twitter;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterFollowJson extends TwitterActionJson implements NetworkActionJson<Twitter> {

    public static final String ACTION_NAME = "follow";

    private String targetUserId;

    public TwitterFollowJson(String id,
                             String targetUserId) {
        super(id);
        this.action = ACTION_NAME;
        this.targetUserId = targetUserId;
    }

    public String getTargetUserId() {
        return targetUserId;
    }
}

