package me.relevante.web.model.json;

import me.relevante.model.TwitterProfile;
import me.relevante.network.Twitter;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class TwitterLoggedUserJsonMapper implements NetworkLoggedUserJsonMapper<Twitter, TwitterProfile> {

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public JSONObject mapNetworkProfileToJson(TwitterProfile profile) {
        JSONObject jsonProfile = new JSONObject();
        jsonProfile.put("screenNameTwitter", profile.getScreenName());
        jsonProfile.put("nameTwitter", profile.getName());
        jsonProfile.put("location", profile.getLocation());
        jsonProfile.put("picture", profile.getImageUrl());
        jsonProfile.put("profile_url", profile.getProfileUrl());
        return jsonProfile;
    }
}