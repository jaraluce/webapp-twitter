package me.relevante.web.model.json;

import me.relevante.network.Twitter;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterReplyJson extends TwitterActionJson implements NetworkActionJson<Twitter> {

    public static final String ACTION_NAME = "reply";

    private String tweetId;
    private String replyText;

    public TwitterReplyJson(String id,
                            String tweetId,
                            String replyText) {
        super(id);
        this.action = ACTION_NAME;
        this.tweetId = tweetId;
        this.replyText = replyText;
    }

    public String getTweetId() {
        return tweetId;
    }

    public String getReplyText() {
        return replyText;
    }
}
