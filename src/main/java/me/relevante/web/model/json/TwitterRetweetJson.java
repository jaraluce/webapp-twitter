package me.relevante.web.model.json;

import me.relevante.network.Twitter;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterRetweetJson extends TwitterActionJson implements NetworkActionJson<Twitter> {

    public static final String ACTION_NAME = "retweet";

    private String tweetId;

    public TwitterRetweetJson(String id,
                              String tweetId) {
        super(id);
        this.action = ACTION_NAME;
        this.tweetId = tweetId;
    }

    public String getTweetId() {
        return tweetId;
    }

}
