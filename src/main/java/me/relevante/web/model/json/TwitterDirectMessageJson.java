package me.relevante.web.model.json;

import me.relevante.network.Twitter;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterDirectMessageJson extends TwitterActionJson implements NetworkActionJson<Twitter> {

    public static final String ACTION_NAME = "directMessage";

    private String targetUserId;
    private String messageText;

    public TwitterDirectMessageJson(String id,
                                    String targetUserId,
                                    String messageText) {
        super(id);
        this.action = ACTION_NAME;
        this.targetUserId = targetUserId;
        this.messageText = messageText;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public String getMessageText() {
        return messageText;
    }
}

