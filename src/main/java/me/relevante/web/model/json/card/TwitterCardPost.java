package me.relevante.web.model.json.card;

import me.relevante.network.Network;
import me.relevante.network.Twitter;

public class TwitterCardPost implements NetworkCardPost {

    private String id;
    private String date;
    private String latitude;
    private String longitude;
    private String lang;
    private String text;
    private String reply;
    private String retweet;
    private String fav;
    private String url;
    private String post_img;

    @Override
    public Network getNetwork() {
        return Twitter.getInstance();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getRetweet() {
        return retweet;
    }

    public void setRetweet(String retweet) {
        this.retweet = retweet;
    }

    public String getFav() {
        return fav;
    }

    public void setFav(String fav) {
        this.fav = fav;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return post_img;
    }

    public void setImageUrl(String post_img) {
        this.post_img = post_img;
    }
}
