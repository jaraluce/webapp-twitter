package me.relevante.web.model.json;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public class TwitterRetweetRequest {

    private String tweetId;

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }
}
