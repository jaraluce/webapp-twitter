package me.relevante.web.model.oauth;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.api.TwitterApiException;
import me.relevante.network.Twitter;

import java.util.Arrays;
import java.util.List;

public class TwitterOAuthManagerProxy implements NetworkOAuthManager<Twitter> {

    private TwitterOAuthManager mainOAuthManager;
    private TwitterOAuthManager extraOAuthManager;
    private TwitterOAuthManager emergencyOAuthManager;
    private List<TwitterOAuthManager> readPool;

    public TwitterOAuthManagerProxy(OAuthKeyPair<Twitter> oAuthMainConsumerKeyPair,
                                    OAuthKeyPair<Twitter> oAuthExtraConsumerKeyPair,
                                    OAuthKeyPair<Twitter> oAuthEmergencyConsumerKeyPair) {
        this.mainOAuthManager = new TwitterOAuthManager(oAuthMainConsumerKeyPair);
        this.extraOAuthManager = new TwitterOAuthManager(oAuthExtraConsumerKeyPair);
        this.emergencyOAuthManager = new TwitterOAuthManager(oAuthEmergencyConsumerKeyPair);
        this.readPool = Arrays.asList(mainOAuthManager, extraOAuthManager, emergencyOAuthManager);
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public OAuthKeyPair<Twitter> getOAuthConsumerKeyPair() throws TwitterApiException {
        return obtainReadOAuthManager().getOAuthConsumerKeyPair();
    }

    @Override
    public OAuthAccessRequest<Twitter> createOAuthRequest(String callbackUrl) throws TwitterApiException {
        TwitterOAuthManager oAuthManager = obtainReadOAuthManager();
        try {
            OAuthAccessRequest<Twitter> response = oAuthManager.createOAuthRequest(callbackUrl);
            return response;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return createOAuthRequest(callbackUrl);
        }
    }

    @Override
    public OAuthTokenPair obtainAccessToken(String requestTokenString,
                                            String requestSecret,
                                            String requestVerifierString) throws TwitterApiException {
        TwitterOAuthManager oAuthManager = obtainReadOAuthManager();
        try {
            OAuthTokenPair<Twitter> response = oAuthManager.obtainAccessToken(requestTokenString, requestSecret, requestVerifierString);
            return response;
        } catch (TwitterApiException e) {
            processReadTwitterApiException(e);
            return obtainAccessToken(requestTokenString, requestSecret, requestVerifierString);
        }
    }

    public OAuthKeyPair<Twitter> getMainOAuthConsumerKeyPair() {
        return mainOAuthManager.getOAuthConsumerKeyPair();
    }

    public OAuthKeyPair<Twitter> getExtraOAuthConsumerKeyPair() {
        return extraOAuthManager.getOAuthConsumerKeyPair();
    }

    public OAuthKeyPair<Twitter> getEmergencyOAuthConsumerKeyPair() {
        return emergencyOAuthManager.getOAuthConsumerKeyPair();
    }

    private TwitterOAuthManager obtainReadOAuthManager() throws TwitterApiException {
        if (readPool.isEmpty()) {
            throw new TwitterApiException("Read credentials pool is exhausted");
        }
        TwitterOAuthManager result = readPool.get(0);
        return result;
    }

    private void processReadTwitterApiException(TwitterApiException e) throws TwitterApiException {
        if (e.getRetryAfter() >= 0) {
            readPool.remove(0);
            return;
        }
        TwitterOAuthManager currentTwitterApiAdapter = readPool.get(0);
        this.readPool.remove(currentTwitterApiAdapter);
    }

}
