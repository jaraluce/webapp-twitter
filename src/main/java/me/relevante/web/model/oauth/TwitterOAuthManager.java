package me.relevante.web.model.oauth;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.api.TwitterApiException;
import me.relevante.network.Twitter;
import org.springframework.beans.factory.annotation.Qualifier;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TwitterOAuthManager implements NetworkOAuthManager<Twitter> {

    private OAuthKeyPair<Twitter> oAuthConsumerKeyPair;

    public TwitterOAuthManager(@Qualifier(value = "twitterMainOAuthKeyPair") OAuthKeyPair<Twitter> oAuthConsumerKeyPair) {
        this.oAuthConsumerKeyPair = oAuthConsumerKeyPair;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    public OAuthKeyPair<Twitter> getOAuthConsumerKeyPair() {
        return this.oAuthConsumerKeyPair;
    }

    @Override
    public OAuthAccessRequest<Twitter> createOAuthRequest(String callbackUrl) throws TwitterApiException {

        RequestToken requestToken = obtainRequestToken(callbackUrl);
        if (requestToken == null) {
            return null;
        }
        OAuthAccessRequest<Twitter> OAuthAccessRequest = new OAuthAccessRequest<>(requestToken.getToken(),
                requestToken.getTokenSecret(), requestToken.getAuthenticationURL());

        return OAuthAccessRequest;
    }

    @Override
    public OAuthTokenPair obtainAccessToken(String requestTokenString,
                                            String requestSecret,
                                            String requestVerifierString) throws TwitterApiException {

        RequestToken requestToken = new RequestToken(requestTokenString, requestSecret);
        twitter4j.Twitter twitter4j = createTwitter4j(oAuthConsumerKeyPair);
        try {
            AccessToken accessToken = twitter4j.getOAuthAccessToken(requestToken, requestVerifierString);
            OAuthTokenPair oAuthTokenPair = new OAuthTokenPair(accessToken.getToken(), accessToken.getTokenSecret());
            return oAuthTokenPair;
        } catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    private RequestToken obtainRequestToken(String callbackUrl) throws TwitterApiException {

        twitter4j.Twitter twitter4j = createTwitter4j(oAuthConsumerKeyPair);
        try {
            RequestToken requestToken = twitter4j.getOAuthRequestToken(callbackUrl);
            return requestToken;
        } catch (TwitterException e) {
            throw new TwitterApiException(e);
        }
    }

    private twitter4j.Twitter createTwitter4j(OAuthKeyPair oAuthConsumerKeyPair) {
        twitter4j.Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(oAuthConsumerKeyPair.getKey(), oAuthConsumerKeyPair.getSecret());
        return twitter;
    }

}
