package me.relevante.web.model.search;

import me.relevante.model.NetworkSignal;
import me.relevante.model.SignalCategory;
import me.relevante.model.TwitterFullPost;
import me.relevante.model.TwitterFullUser;
import me.relevante.model.TwitterSearchUser;
import me.relevante.network.Twitter;
import me.relevante.nlp.TwitterStemUserIndex;
import me.relevante.nlp.core.NlpCore;
import me.relevante.web.persistence.TwitterFullPostRepo;
import me.relevante.web.persistence.TwitterFullUserRepo;
import me.relevante.web.persistence.TwitterSearchUserRepo;
import me.relevante.web.persistence.TwitterStemUserIndexRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class TwitterSearcher extends AbstractNetworkSearcher<Twitter, TwitterFullUser, TwitterSearchUser, TwitterStemUserIndex>
        implements NetworkSearcher<Twitter, TwitterSearchUser> {

    private TwitterSearchUserRepo searchUserRepo;
    private TwitterStemUserIndexRepo stemUserIndexRepo;
    private TwitterFullUserRepo fullUserRepo;
    private TwitterFullPostRepo fullPostRepo;

    @Autowired
    public TwitterSearcher(NlpCore nlpCore,
                           TwitterSearchUserRepo searchUserRepo,
                           TwitterStemUserIndexRepo stemUserIndexRepo,
                           TwitterFullUserRepo fullUserRepo,
                           TwitterFullPostRepo fullPostRepo) {
        super(nlpCore);
        this.searchUserRepo = searchUserRepo;
        this.stemUserIndexRepo = stemUserIndexRepo;
        this.fullUserRepo = fullUserRepo;
        this.fullPostRepo = fullPostRepo;
    }

    @Override
    public Twitter getNetwork() {
        return Twitter.getInstance();
    }

    @Override
    protected List<TwitterStemUserIndex> findStemUserIndexByStem(String stem, int maxUsers) {
        return stemUserIndexRepo.findByStemOrderByScoreDesc(stem, new PageRequest(0, maxUsers));
    }

    @Override
    protected List<TwitterSearchUser> findSearchUsersById(List<String> ids) {
        return searchUserRepo.findByIdIn(ids);
    }

    @Override
    protected List<TwitterFullUser> findFullUsersById(List<String> ids) {
        return fullUserRepo.findByIdIn(ids);
    }

    @Override
    protected void assignPostsToUsers(List<TwitterSearchUser> searchUsers,
                                      int maxPosts) {
        // Create set for each user containing post IDs from their signals
        List<LinkedHashSet<String>> allUsersPostIds = new ArrayList<>();
        for (TwitterSearchUser searchUser : searchUsers) {
            LinkedHashSet<String> postIds = new LinkedHashSet();
            allUsersPostIds.add(postIds);
            for (NetworkSignal signal : searchUser.getSignals()) {
                if (signal.getCategory().equals(SignalCategory.CONTENT_ACTIVITY)) {
                    postIds.add(signal.getRelatedEntityId());
                }
            }
        }
        Set<String> allPostIds = new HashSet<>();
        for (int i = 0; i < allUsersPostIds.size(); i++) {
            // Add all Post IDs from the full user
            LinkedHashSet<String> userPostIds = allUsersPostIds.get(i);
            List<String> fullUserPostIds = searchUsers.get(i).getFullUser().getPostIds();
            userPostIds.addAll(fullUserPostIds);
            // Leave posts to the max allowed
            List<String> maxAllowedUserPostIds = new ArrayList<>();
            Iterator<String> iterator = userPostIds.iterator();
            for (int j = 0; j < maxPosts; j++) {
                if (!iterator.hasNext())
                    break;
                String postId = iterator.next();
                maxAllowedUserPostIds.add(postId);
                allPostIds.add(postId);
            }
            userPostIds.clear();
            userPostIds.addAll(maxAllowedUserPostIds);
        }

        List<TwitterFullPost> fullPosts = fullPostRepo.findByIdIn(allPostIds);
        Map<String, TwitterFullPost> fullPostsById = new HashMap<>();
        fullPosts.forEach(fullPost -> fullPostsById.put(fullPost.getId(), fullPost));
        for (int i = 0; i < searchUsers.size(); i++) {
            TwitterSearchUser searchUser = searchUsers.get(i);
            LinkedHashSet<String> postIds = allUsersPostIds.get(i);
            Iterator<String> iterator = postIds.iterator();
            searchUser.getPosts().clear();
            while (iterator.hasNext()) {
                String postId = iterator.next();
                TwitterFullPost fullPost = fullPostsById.get(postId);
                searchUser.getPosts().add(fullPost);
            }
        }
    }

}
