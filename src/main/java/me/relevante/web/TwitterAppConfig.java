package me.relevante.web;

import me.relevante.api.OAuthKeyPair;
import me.relevante.network.Twitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by daniel-ibanez on 24/07/16.
 */
@Configuration
public class TwitterAppConfig {

    @Bean(name = "twitterMainOAuthKeyPair")
    public OAuthKeyPair<Twitter> twitterMainOAuthKeyPair(@Value("${twitter.oAuthCredentials.main.apiKey}") String key,
                                                         @Value("${twitter.oAuthCredentials.main.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

    @Bean(name = "twitterExtraOAuthKeyPair")
    public OAuthKeyPair<Twitter> twitterExtraOAuthKeyPair(@Value("${twitter.oAuthCredentials.extra.apiKey}") String key,
                                                          @Value("${twitter.oAuthCredentials.extra.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

    @Bean(name = "twitterEmergencyOAuthKeyPair")
    public OAuthKeyPair<Twitter> twitterEmergencyOAuthKeyPair(@Value("${twitter.oAuthCredentials.emergency.apiKey}") String key,
                                                              @Value("${twitter.oAuthCredentials.emergency.apiSecret}") String secret) {
        return new OAuthKeyPair<>(key, secret);
    }

}
